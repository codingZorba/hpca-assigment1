#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>
#include <fstream>
using namespace std;

#define FILTER_SIZE 3
#define FILTERS 2

// Optimize this function
void applyFilters(int *image, int height, int width, int *filter, int filters)
{
    for(int i = 0; i < filters; ++i)
    {
        int *copy = new int[height * width]();
        //Three cases for height = 0
		//height = 0 and width = 0
				for(int offx = 0; offx <= 1; ++offx)
                {
                    for(int offy = 0; offy <= 1; ++offy)
                    {
						copy[0* width + 0] += image[(offx + 0) * width + offy + 0] * 
						filter[i * FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
                    }
                }
		//height = 0 and width = width-1
				for(int offx = 0; offx <= 1; ++offx)
                {
                    for(int offy = -1; offy <= 0; ++offy)
                    {
						copy[0 * width + (width-1)] += image[(offx + 0) * width + offy + (width-1)] * 
						filter[i * FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
                    }
                }
		//height = 0 and width between 0 and width-1
				for(int offx = 0; offx <= 1; ++offx)
                {
                    for(int offy = -1; offy <= 1; ++offy)
                    {
						for(int w = 1; w < width-1; ++w)
						{
							copy[0 * width + w] += image[(offx + 0) * width + offy + w] * 
							filter[i * FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
						}
                    }
                }

// height between 0 and height-1
        for(int h = 1; h < height-1; ++h)
        {  
				//width = 0
				for(int offx = -1; offx <= 1; ++offx)
                {
                    for(int offy = 0; offy <= 1; ++offy)
                    {
						copy[h * width + 0] += image[(offx + h) * width + offy + 0] * 
						filter[i * FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
                    }
                }
				//for the middle matrix without corner cases                
				for(int offx = -1; offx <= 1; ++offx)
                {
                    for(int offy = -1; offy <= 1; ++offy)
                    {
						for(int w = 1; w < width-1; ++w)
						{
							copy[h * width + w] += image[(offx + h) * width + offy + w] * 
							filter[i * FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
						}
                    }
                }
				//width = width-1
            	for(int offx = -1; offx <= 1; ++offx)
                {
                    for(int offy = -1; offy <= 0; ++offy)
                    {
							copy[h * width + (width-1)] += image[(offx + h) * width + offy + (width-1)] * 
							filter[i * FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
                    }
                }
        }


        //Three cases for height = height-1
		//height = height-1 and width = 0
				for(int offx = -1; offx <= 0; ++offx)
                {
                    for(int offy = 0; offy <= 1; ++offy)
                    {
						copy[(height-1) * width + 0] += image[(offx + (height-1)) * width + offy + 0] * 
						filter[i * FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
                    }
                }
		//height = height-1 and width = width-1
				for(int offx = -1; offx <= 0; ++offx)
                {
                    for(int offy = -1; offy <= 0; ++offy)
                    {
						copy[(height-1) * width + (width-1)] += image[(offx + (height-1)) * width + offy + (width-1)] * 
						filter[i * FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
                    }
                }
		//height = height-1 and width between 0 and width-1
				for(int offx = -1; offx <= 0; ++offx)
                {
                    for(int offy = -1; offy <= 1; ++offy)
                    {
						for(int w = 1; w < width-1; ++w)
						{
							copy[(height-1) * width + w] += image[(offx + (height-1)) * width + offy + w] * 
							filter[i * FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
						}
                    }
                }


	for(int h = 0; h < height; ++h)
        {
            for(int w = 0; w < width; ++w)
                image[h * width + w] = copy[h * width + w];
        }
        delete copy;
    }
}

int main()
{
    int ht, wd;
    cin >> ht >> wd;
    int *img = new int[ht * wd];
    for(int i = 0; i < ht; ++i)
        for(int j = 0; j < wd; ++j)
            cin >> img[i * wd + j];
            
    int filters = FILTERS;
    int *filter = new int[filters * FILTER_SIZE * FILTER_SIZE];
    for(int i = 0; i < filters; ++i)
        for(int j = 0; j < FILTER_SIZE; ++j)
            for(int k = 0; k < FILTER_SIZE; ++k)
                cin >> filter[i * FILTER_SIZE * FILTER_SIZE + j * FILTER_SIZE + k];
    
    clock_t begin = clock();
    applyFilters(img, ht, wd, filter, filters);
    clock_t end = clock();
    cout << "Execution time: " << double(end - begin) / (double)CLOCKS_PER_SEC << " seconds\n";
    ofstream fout("output.txt");
    for(int i = 0; i < ht; ++i)
    {
        for(int j = 0; j < wd; ++j)
            fout << img[i * wd + j] << " ";
        fout << "\n";
    }
}